from generic_relations.relations import GenericRelatedField
from rest_framework.serializers import (
    ModelSerializer, SerializerMethodField, HyperlinkedRelatedField)

from .models import Book, Review


class ReviewSerializer(ModelSerializer):
    # will show a full URL from domain.
    # to write you will need to pass a URL
    # after the domain
    # ex) /api/1/
    tagged_object = GenericRelatedField({
        Book: HyperlinkedRelatedField(
            queryset=Book.objects.all(),
            view_name='books:book_detail',
        ),
    })

    class Meta:
        model = Review
        fields = [
            'review',
            'author',
            'tagged_object'
        ]


class BookSerializer(ModelSerializer):
    reviews = ReviewSerializer(many=True)

    class Meta:
        model = Book
        fields = [
            'id',
            'title',
            'author',
            'price',
            'reviews',
        ]
