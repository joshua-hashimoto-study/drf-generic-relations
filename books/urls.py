from django.urls import path

from .views import (BookListCreateAPIView, BookRetrieveUpdateDestroyAPIView,
                    ReviewListCreateAPIView, ReviewRetrieveUpdateDestroyAPIView)

app_name = 'books'

urlpatterns = [
    path('', BookListCreateAPIView.as_view(), name='book_list'),
    path('<int:pk>/', BookRetrieveUpdateDestroyAPIView.as_view(),
         name='book_detail'),
    path('review/', ReviewListCreateAPIView.as_view(), name='review_list'),
    path('review/<int:pk>/', ReviewRetrieveUpdateDestroyAPIView.as_view(),
         name='review_detail'),
]
